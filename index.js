// const express = require('express');
// const app = express();
// const port = 3000;

// app.get('/', (req, res) => {
//   res.send('Hello World!');
// });
        
// app.listen(port, () => {
//   console.log(`Example app listening at http://localhost:${port}`);
// });



// const mongoose = require('mongoose');
// const express = require('express');
// const dotenv = require('dotenv');
// const jwt = require('jsonwebtoken');

// const connectMongo = require("./config/dbConfig");

// const app = express();
// app.use(express.json())

// const users=[
//     {
//         id:"1",
//         username:"rahul",
//         password:"rahul123",
//         isAdmin:"true",
//     },
//     {
//         id:"2",
//         username:"roar",
//         password:"roar123",
//         isAdmin:"false",
//     },
// ];

// // Set up Global configuration access
// dotenv.config();


// app.post("/api/refresh", (req, res) => {
//     //take the refresh token from the user
//     const refreshToken = req.body.token;
  
//     //send error if there is no token or it's invalid
    
//   });


// app.post("/api/login", (req,res)=>{
//     const {username, password}=req.body
//     const user = users.find(u=>{
//         return u.username === username && u.password == password
//     })
//     if(user){
//         //res.json(user)

//         //generating accesstoken
//         const accessToken = jwt.sign({id:user.id, isAdmin:user.isAdmin},"secretkey")
//         res.json({
//             username: user.username,
//             isAdmin: user.isAdmin,
//             accessToken,
//         })
//     }else{
//         res.status(400).json("Incorrect username or password")
//     }

//     //res.json("it works")
// });

// const verify = (req, res, next) => {
//     const authHeader = req.headers.authorization
//     if(authHeader){
//         const token = authHeader.split(" ")[1]

//         jwt.verify(token,"secretkey",(err,user)=>{
//             if(err){
//                 return res.status(401).json("Token not valid")
//             }

//             req.user=user
//             next()
//         })
//     }else{
//         res.status(401).json("not authenticated")
//     }
// }
// app.delete("/api/users/:userId", verify, (req,res)=>{
//     if(req.user.id === req.params.userId || req.user.isAdmin){
//         res.status(200).json("user deleted")
//     }else{
//         res.status(403).json("not allowed to delete user")
//     }
// })

// app.listen(5000,()=> console.log("Server is running in port 5000"))
// connectMongo.connectDB();





// Main Code Here //
// Generating JWT
// app.post("/user/generateToken", (req, res) => {
// 	// Validate User Here
// 	// Then generate JWT Token

// 	let jwtSecretKey = process.env.JWT_SECRET_KEY;
// 	let data = {
// 		time: Date(),
// 		userId: 12,
// 	}

// 	const token = jwt.sign(data, jwtSecretKey);

// 	res.send(token);
// });

// Verification of JWT
// app.get("/user/validateToken", (req, res) => {
// 	// Tokens are generally passed in header of request
// 	// Due to security reasons.

// 	let tokenHeaderKey = process.env.TOKEN_HEADER_KEY;
// 	let jwtSecretKey = process.env.JWT_SECRET_KEY;

// 	try {
// 		const token = req.header(tokenHeaderKey);

// 		const verified = jwt.verify(token, jwtSecretKey);
// 		if(verified){
// 			return res.send("Successfully Verified");
// 		}else{
// 			// Access Denied
// 			return res.status(401).send(error);
// 		}
// 	} catch (error) {
// 		// Access Denied
// 		return res.status(401).send(error);
// 	}
// });













const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const logger = require("morgan");
const mongoose = require("mongoose");

mongoose.set("useNewUrlParser", true);
mongoose.set("useFindAndModify", false);
mongoose.set("useCreateIndex", true);

const port = 3031;
const config = require("./config/dbConfig");

const postsRouter = require("./routes/post");

app.use(logger("dev"));

const dbUrl = config.dbUrl;

var options = {
  keepAlive: 1,
  connectTimeoutMS: 30000,
  useNewUrlParser: true,
  useUnifiedTopology: true,
};

mongoose.connect(dbUrl, options, (err) => {
  if (err) console.log(err);
});

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use("/post", postsRouter);

app.listen(port, function () {
  console.log("Runnning on " + port);
});
module.exports = app;